package ictgradschool.industry.lab15.ex01;

import java.util.ArrayList;
import java.util.List;

public class NestingShape extends Shape {

    List<Shape> nestingShapeList = new ArrayList<>();

    public NestingShape(){
       super();

    }

    public NestingShape(int x, int y){
        super(x,y);

    }

    public NestingShape(int x, int y, int deltaX, int deltaY){
        super(x, y, deltaX, deltaY);

    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height){
        super(x,y,deltaX,deltaY,width,height);

    }
    public void move(int width, int height){
        super.move(width, height);

        for (Shape s: nestingShapeList){
            s.move(this.fWidth, this.fHeight);
        }

    }
    public void paint(Painter painter){
        painter.drawRect(fX,fY,fWidth,fHeight);

        painter.translate(fX, fY);

        for (Shape s: nestingShapeList){
            s.paint(painter);
        }

        painter.translate(-fX,-fY);
    }

    public void add(Shape child) throws IllegalArgumentException{

        if (!nestingShapeList.contains(child)){
            nestingShapeList.add(child);
            child.parent = this;
        }else if (nestingShapeList.contains(child)){
            throw new IllegalArgumentException("NestingShape already contains Child Shape.");
        }
    }

    public void remove(Shape child){
        if (nestingShapeList.contains(child)){
            nestingShapeList.remove(child);
            child.parent = null;
        }
    }

    public Shape shapeAt(int index) throws IndexOutOfBoundsException {

        if(index < nestingShapeList.size()){
            return nestingShapeList.get(index);
        }
        else {
            throw new IndexOutOfBoundsException("Index value is greater than List length.");
        }

    }

    public int shapeCount(){
        return nestingShapeList.size();

    }

    public int indexOf (Shape child){
        if (nestingShapeList.contains(child)){
            return indexOf(child);
        }
        return -1;

    }

    public boolean contains(Shape child){
        return nestingShapeList.contains(child);

    }

}

