package ictgradschool.industry.lab12.bounce;

import java.awt.*;

public class GemShape extends Shape {

    private Polygon polygonA;
    private final int width;
    private final int height;

    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
        this.width = width;
        this.height = height;
        createPoly();
    }

    private void createPoly() {
        if (width <= 40) {
            int[] polyX = {fX + this.width / 2, fX + this.width, fX + this.width / 2, fX + 0, fX + this.width/2};
            int[] polyY = {fY + 0, fY + this.height/2, fY + this.height, fY + this.height/2,fY + 0};


            polygonA = new Polygon(polyX, polyY, polyX.length);


        } else {
            int[] polyX = {fX + 20, fX + this.width-20, fX + this.width, fX + this.width-20, fX + 20, fX + 0, fX + 20};
            int[] polyY = {fY + 0, fY + 0, fY + this.height/2, fY + this.height, fY + this.height, fY + this.height/2, fY + 0};


            polygonA = new Polygon(polyX, polyY, polyX.length);
        }
    }

    @Override
    public void move(int width, int height) {
        super.move(width, height);
        createPoly();
    }

    @Override
    public void paint(Painter painter) {
        painter.drawPolygon(polygonA);
    }
}



































