package ictgradschool.industry.lab12.bounce;

import java.awt.*;

public class DynamicRectangleShape extends Shape {
    Color fillColor = Color.black;
    boolean toggleFill = true;

    public DynamicRectangleShape() {
        super();
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY) {
        super(x,y,deltaX,deltaY);
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, Color fill) {
        super(x,y,deltaX,deltaY);
        // if(deltaX > 0 | deltaY > 0){
        fillColor = fill;
            /*}
            else if (deltaX < 0 | deltaY < 0){
                fillColor = Color.black;
            }*/


    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
    }

    int xDir, yDir = 1;
    @Override
    public void paint(Painter painter) {
        int newXDir = (fDeltaX < 0) ? -1 : 1;
        int newYDir = (fDeltaY < 0) ? -1 : 1;

        if (xDir != newXDir || yDir != newYDir) {
            // We have changed direction
            toggleFill = !toggleFill;
        }

        xDir = newXDir;
        yDir = newYDir;


        if (toggleFill) {
            painter.setColor(fillColor);
            painter.fillRect(fX, fY, fWidth, fHeight);
        }

        painter.setColor(Color.black);
        painter.drawRect(fX,fY,fWidth,fHeight);
    }







}
