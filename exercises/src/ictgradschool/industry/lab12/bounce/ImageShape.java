package ictgradschool.industry.lab12.bounce;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class ImageShape extends Shape {

    private Image image1;

    public ImageShape() {

        super();
    }

    public ImageShape(int x, int y, int deltaX, int deltaY) {

        super(x, y, deltaX, deltaY);
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) {
        super(x, y, deltaX, deltaY, width, height);
        try {
            image1 = ImageIO.read(new File(fileName));
        }
        catch (IOException e){
            e.printStackTrace();

        }

    }

    @Override
    public void paint(Painter painter) {
        painter.drawImage( this.image1,fX, fY, fWidth, fHeight);
    }


}
